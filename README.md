# The Resurrected Web

This is the code repository for the resurrected web, a solar powered website made collaboratively with Fablab BCN 2021 students in the Code Club.
Main content is [here](https://fablabbcn-projects.gitlab.io/learning/fabacademy-local-docs/clubs/codeclub/theresurrectedweb/)

## Structure

- [site](site/) is where we'll put our static site
- [server](server/) is were we'll put our server configuration
- [test](test/) is where we'll document our test results
- [assets](assets/) pictures, media files and so on