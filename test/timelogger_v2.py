import datetime
from time import sleep

#Initialize boolean of last line detection
bBootLine = 0
bInit = 0
#Running through the file to check the BOOT events
with open("/home/pi/log_v2.txt",mode='r') as file:
	#Go through the file
	for last_line in file:
		if last_line != "":
			bInit = 1
			#if a line with "BOOT" was detected at previous step, we store it.
			if bBootLine == 1:
				bootLine = last_line
			#if the actual step contains BOOT we set up bBootline for saving next step
			if last_line == "BOOT\n":
				bBootLine = 1
			else:
				bBootLine = 0
			#Go to the next line
		else:
			#Used to avoid issue when writing the file for the first time
			bInit = 0
		pass
	file.close
if bInit == 1:
	time_boot = datetime.datetime.strptime(str.rstrip(bootLine), '%Y-%m-%d %H:%M:%S.%f') #the function is used to retrieve back a number from the string
	time_stop = datetime.datetime.strptime(str.rstrip(last_line),'%Y-%m-%d %H:%M:%S.%f')
        #time_boot = str.rstrip(bootLine)
	#time_stop = str.rstrip(last_line)
	time_elapse = time_stop-time_boot
	#Writing the elapsed time to the file
	with open("/home/pi/log_v2.txt", mode='a') as file:
		file.write("Time Up = %s\n" %time_elapse)
		file.close
#Adding bootin event to the file
with open("/home/pi/log_v2.txt",mode='a') as file:
	file.write('BOOT\n')
	file.close
sleep(60)
while True:
        with open("/home/pi/log_v2.txt", mode='a') as file:
                file.write('%s\n' % 
                        (datetime.datetime.now()))
        sleep (60)
