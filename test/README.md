# Tests

This folder contains the tests we have been performing for the hardware/software options.
Instructions:

1. Upload a folder with pictures and readme to this folder
2. Add the link to that folder in this readme

## Test list

### Running timelogger_v2.py at BOOT

#### RC.LOCAL Option

The timelogger_v2.py will:  

1. Write a time stamp every minute the Pi is ON
2. Count the time elapsed during the last "ON" period and note it down

The info is written in log_v2.txt 

To make it run  

1. Copy timelogger_v2.py in your pi at /home/pi/
2. Create an empty file log_v2.txt
3. Run the following command to open the "boot commands"
    ```
    sudo nano /etc/rc.local
    ```
4. In the file that opens past the following line : 
    ```
    sudo python /home/pi/timelogger_v2.py
    ```

!!! tip
    All the roots and file name can be adapted to your project.

!!!danger
    We discovered some issues 
    
    * If no internet the time given is not correct -> we should try to improve the launch process (services ?)
    * When shutting down, we get bad time information (^@^@^@) which crashes the script
    * Sometime the script does not restart by itself

#### Systemd option

Write this unit file in `/etc/systemd/system`. Make sure to start it with `sudo`:

```
[Unit]
Description=Logging pi time up
After=network.target

[Service]
Type=simple
User=pi
Group=pi
WorkingDirectory=/home/pi
ExecStart=/usr/bin/python /home/pi/script.py
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target
```

Some useful commands:

- Enable the service (this enables the service, will run next time the pi boots)
```
sudo systemctl enable logging_time --> 
```

- Start the service (this starts running the service, will run inmediately)
```
sudo systemctl start logging_time
```

- Check the log:
```
journalctl -u logging_time.service -f
```

- Stop it (will run again next boot)
```
sudo systemctl stop logging_time
```

- Disable it the service:
```
sudo systemctl disable logging_time
```

## Test summary

### We are Cheap Energy
![diagram](../assets/cheap_energy/cheap_energy_solar_website.png)

### We are SolarPi
![Our Setup](../assets/solarpi/solarpi_setup_data.jpg)
